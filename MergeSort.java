//etcheverrypablol@gmail.com
// github/gitlab: etcheverrypablol
// Algoritmo de MegeSort implementado en JAVA en una lista de enteros
// Video de ejemplo: https://www.youtube.com/watch?v=Xhlf6f26M7Y

public class MergeSort{
		static final int Max = 8;
	public static void main (String[] args){
		System.out.println("PROGRAMA: IMPLEMENTACION DE MERGESORT EN UNA LISTA DE ENTEROS.");

		Integer a[] = new Integer[] {0,2,7,6,0,-40,30,200,1}; // Array con úlitmo elemento en a[Max]
		Integer b[] = new Integer[Max+1]; // Observar que el array tiene 8 elementos (de 0 a 7)

		System.out.println("Lista antes de ser ordenada: ");
		printArray(a,Max);
		mergeSort(0,Max,a,b);
		System.out.println("Lista despues de ser ordenada: ");
		printArray(a,Max);

	}

	// Procedimeinto que divide el arreglo a la mitad recurdivamente y 
	// luego llama a la acción que los va a juntar de manera ordenada (merge)
	public static void mergeSort(int low, int high, Integer a[], Integer b[]){
		int mid;
		if(low<high){
			mid = (low+high)/2;
			mergeSort(low,mid,a,b);
			mergeSort(mid+1,high,a,b);
			merge(low,mid,high,a,b);
		}else{
			return;
		}

	}	
 	// Procedimiento que junta los "arreglo divididos" de manera ordenada
	public static void merge(int low, int mid, int high, Integer a[], Integer b[]){
		int l1,l2,i;
		l1=low;
		l2=mid+1;
		i=low;

		while(l1<=mid && l2<=high){
			if(a[l1]<=a[l2]){
				b[i]=a[l1];
				i=i+1; l1=l1+1;
			}else{
				b[i]=a[l2];
				i=i+1; l2=l2+1;
			}	
		}
		//{Estado: La primera mitad del arreglo b queda ya más ordenada}
		while(l1<=mid){
			b[i]=a[l1];
			l1=l1+1;
			i=i+1;
		}
		while(l2<=high){
			b[i]=a[l2];
			l2=l2+1;
			i=i+1;
		}
		//{Estado: Con esos dos ciclos, copio en la segunda mitad de b los elementos restantes}
		for(i=0;i<=high;i++){
			a[i]=b[i];
		}
		//{Estado: queda en en el arreglo a la lista un poco más ordenada}
	}
	// Procedimiento que imprime un array
	public static void printArray(Integer a[], int n){
		for(int i=0; i<=n; i++){
			System.out.print(a[i]+" ");
		}
		System.out.println();
	}
}

