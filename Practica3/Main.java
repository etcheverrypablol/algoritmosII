//etcheverrypablol@gmail.com
// github/gitlab: etcheverrypablol
// Ejercicio de inversiones. La resolucion del ejercicio es muy similar a la del algoritmo merge sort
// teniendo en cuenta que cada vez que se va a colocar un elemento del subarreglo de la derecha en el 
// arreglo de ayuda, tenemos que sumar el numero de elementos que falta por analizar en el arreglo 
// izquierdo a la suma que ya trae inversiones.

public class Main{
		static final int Max = 8;
		

	public static void main (String[] args){
		Integer inv = 0;	// Para pasar como parámetro por referencia es necesario usar Integer antes que int

		System.out.println("PROGRAMA: Calcula el numero de inversiones de un arreglo.");

		Integer a[] = new Integer[] {0,2,7,6,0,-40,30,200,1}; // Arreglo con ulitmo elemento en a[Max]
		Integer b[] = new Integer[Max]; // Arreglo donde se va a ir colocando los elementos ordenados 

		System.out.println("Lista antes de calcular inversiones: ");
		printArray(a,Max);
		inversiones(0,Max,a,inv);
		System.out.println("Lista despues de calcular inversiones: ");
		printArray(a,Max);
		System.out.println("El numero de inversiones es: "+inv);


	}

	// Procedimeinto que divide el arreglo a la mitad recursivamente y 
	// luego llama a la acción que compara el numero de inversiones
	public static Integer inversiones(int low, int high, Integer a[]){
		int mid;
		if(low<high){
			mid = (low+high)/2;
			inversiones(low,mid,a,inv) + inversiones(mid+1,high,a,inv) + comparacion(low,mid,high,a,inv);
		}else{
			return;
		}

	}	
 	// Procedimiento que calcula el numero de inversiones
	public static Integer comparacion(int low, int mid, int high, Integer a[]){
		int l1,l2,i;
		l1=low;
		l2=mid+1;
		i=low;
		System.out.println("Se ingreso a comparacion..");
		while(l1<=mid && l2<=high){
			if(a[l1]>a[l2]){
				b[i]=a[l1];
				i++; l1++; inv ++;

				System.out.println("Se incremento inv.."+inv);
			}	
			
		}
	}

	// Procedimiento que imprime un array
	public static void printArray(Integer a[], int n){
		for(int i=0; i<=n; i++){
			System.out.print(a[i]+" ");
		}
		System.out.println();
	}
}

