//etcheverrypablol@gmail.com
// github/gitlab: etcheverrypablol
// Ejercicio de inversiones. La resolucion del ejercicio es muy similar a la del algoritmo merge sort
// teniendo en cuenta que cada vez que se va a colocar un elemento del subarreglo de la derecha en el 
// arreglo de ayuda, tenemos que sumar el numero de elementos que falta por analizar en el arreglo 
// izquierdo a la suma que ya trae inversiones.

public class Inversiones{
		static final int Max = 5;
		

	public static void main (String[] args){
		//Integer inv = 0;	// Para pasar como parámetro por referencia es necesario usar Integer antes que int

		System.out.println("PROGRAMA: Calcula el numero de inversiones de un arreglo.");

		Integer a[] = new Integer[] {2,5,1,0,4,8}; // Arreglo con ulitmo elemento en a[Max]
		Integer b[] = new Integer[Max+1]; // Arreglo donde se va a ir colocando los elementos ordenados 

		System.out.println("Lista antes de calcular inversiones: ");
		printArray(a,Max);
		System.out.println("El numero de inversiones es: "+inversiones(0,Max,a,b));
	}

	// Procedimeinto que divide el arreglo a la mitad recursivamente y 
	// luego llama a la acción que compara el numero de inversiones
	public static Integer inversiones(int low, int high, Integer a[], Integer b[]){
		int mid;
		System.out.println("se ingreso a inversiones..");
		if(low<high){
			mid = (low+high)/2;
			return inversiones(low,mid,a,b) + inversiones(mid+1,high,a,b) + comparacion(low,mid,high,a,b);
		}else{
			System.out.println("retorno 0");
			return 0;	// Observar que no entiendo esto todavía
		}

	}	
 	// Procedimiento que calcula el numero de inversiones
	public static Integer comparacion(int low, int mid, int high, Integer a[], Integer b[]){
		Integer inv=0;	// Variable que va registrando las inversiones en la union de dos subarreglos
		int l1,l2,i;
		l1=low;
		l2=mid+1;
		i=low;
		System.out.println("Se ingreso a comparacion..");
		while(l1<=mid && l2<=high){
			if(a[l1]>a[l2]){
				b[i]=a[l2];
				inv=inv + (l2-l1);	// Antes habia puesto inv= (mid -i)
				i++; l2++;
			}else{
				b[i]=a[l1];
				i++; l1++;
			}	
		}
		while(l1<=mid){
			b[i]=a[l1];
			i++;l1++;
		}	
		while(l2<=high){
			b[i]=a[l2];
			i++;l2++;
		}			
		for(i=0;i<=high;i++){
			a[i]=b[i];
			System.out.print(a[i]+" ");
		}		
		System.out.println();
		System.out.println("Numero de inversiones: "+inv);

		return inv;			
	}

	// Procedimiento que imprime un array
	public static void printArray(Integer a[], int n){
		for(int i=0; i<=n; i++){
			System.out.print(a[i]+" ");
		}
		System.out.println();
	}
}

