// etcheverrypablol@gmail.com
// Algoritmo de ordenamiento MergeSort, implementado en C con un array de enteros
// GitHub/GitLab: etcheverrypablol
// Video de ejemplo: https://www.youtube.com/watch?v=Xhlf6f26M7Y

#include <stdio.h>
#include <stdio.h>
 
#define BgYELLOW "\x1b[43m"
#define YELLOW "\x1b[33m"
#define REST "\x1b[0m"
 

#define Max 8
// Perfiles de las funciones
void merging(int low, int mid, int high);
void mergeSort(int low, int high);

// Arreglo de ejemplo para aplicar MergeSort
int a[Max] = { 8,7,6,5,4,3,10,1};
int b[Max];

int main(int argc, char const *argv[])
{
	printf(BgYELLOW "PROGRAMA: IMPLEMENTACION DEL ALGORITMO MERGESORT EN UNA LISTA DE ENTEROS.\n" REST "\n");
		
	printf("LISTA ANTES DE SER ORDENADA: \n");
	for(int i=0;i<=Max;i++){
		printf("%i ",a[i]);
	}
	printf("\n");

	mergeSort(0,Max);
	
	printf("\nLISTA DESPUES DE SER ORDENADA: \n");
	for(int i=0;i<=Max;i++){
		printf("%i ",a[i]);
	}		
	printf("\n");
	printf(YELLOW "Fin del programa..\n" REST"\n");
	return 0;
}
// Función que divide el arreglo de manera recursiva a la mitad
void mergeSort(int low, int high){
	int mid;
	if(low<high){
		mid=(low+high)/2;
	mergeSort(low,mid);
	mergeSort(mid+1,high);
	merging(low,mid,high);
	}else{
		return;
	}
}
// Función que se encarga de ir uniendo los diferentes "arreglos divididos"
// de una manera ordenada.
void merging(int low, int mid, int high){
	int l1, l2, i;
	// En un solo for incluimos dos variables
	for(l1=low, l2=mid+1, i=low; l1<=mid && l2<=high; i++){  
		if(a[l1]<=a[l2]){ 
			b[i]=a[l1++];
		}else{
			b[i]=a[l2++];
		}
	//{Estado: La primera mitad de b queda un poco más ordenada}
	}
	while(l1<=mid){
			b[i++]=a[l1++];
		}
	while(l2<=high){
		b[i++]=a[l2++];
	}
	//{Estado: Con estos dos ciclos se termina de pasar en b los elemntos restantes}
	for(int i=low ; i<=high; i++){
		a[i]=b[i];
	}
	// {Estado: Se pasa en el arreglo a, lo que ya se ha ordenado en b}
}
