// etcheverrypablol@gmail.com
// github/gitlab: etcheverrypablol
// Algoritmo QuickSort aplicado a una lista de enteros

public class QuickSort{

	static final int Max = 10;
	public static void main(String[] args){
		
		Integer a[] = new Integer[] {3,2,8,5,40,50,-2,800,2,0};

		System.out.println("PROGRAMA: APLICACION DEL ALGORITMO QUICKSORT EN UNA LISTA DE ENTEROS.");
		System.out.println("Arreglo antes de ser ordenado: ");
		for(int i=0; i<Max; i++){
			System.out.print(a[i]+" ");
		}
		System.out.println();
		QuickSort(a,0,Max-1);
		System.out.println("Arreglo luego de ser ordenado: ");
		for(int i=0; i<Max; i++){
			System.out.print(a[i]+" ");
		}
		System.out.println("\nFin del programa..");
	}
	// Procedimiento recursivo que ordena el arreglo
	public static void QuickSort(Integer a[], int low, int high){
		int i,j,pivote;
		Integer tmp;
		j=0;
		if(low<high){
			i=low;
			j=high;
			pivote=low;
			while(i<j){
				while(a[i]<=a[pivote] && i<=high){
					i++;
					System.out.println("incremento i..");
				}
				while(a[j]>a[pivote] && j>=low){
					j--;
					System.out.println("incremento j..");

				}
				if(i<j){
					tmp=a[i];
					a[i]=a[j];
					a[j]=tmp;
					System.out.println("intercambio i j..");

				}
			}
			tmp=a[j];
			a[j]=a[pivote];
			a[pivote]=tmp;
			System.out.println("intercambio pivote..");

			QuickSort(a,low,j-1);
			QuickSort(a,j+1,high);
		}
	}
} 