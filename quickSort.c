// etcheverrypablol@gmail.com
// github/gitlab: etcheverrypablol
// Implementación del algoritmo QuickSort con una lista de enteros

#include <stdio.h>
#define FgYellow "\x1b[43m"
#define BgYellow "\x1b[33m"
#define REST "\x1b[0m"
#define Max 10

int quickSort(int a[] ,int low, int high);

int main(){
	int a[Max] = {3,2,8,5,40,50,-2,800,2,0}; 	//Lista de enteros

	printf(FgYellow "PROGRAMA: USO DEL ALGORITMO QUICKSORT EN UNA LISTA DE ENTEROS.\n" REST "\n" );
	
	printf("Arreglo antes de ser ordenado: \n");
	for(int i=0; i<Max; i++){
		printf("%i ",a[i] );
	}
	printf("\n");
	
	quickSort(a,0,Max-1);

	printf("\nArreglo luego de ser ordenado: \n");
	for(int i=0; i<Max; i++){
		printf("%i ",a[i] );
	}
	printf(BgYellow"\nfin del programa..\n"REST);
	return 0;
} 
// Procedimiento recursivo que ordena la lista

int quickSort(int a[] ,int low, int high){
	printf(BgYellow "se llamo a quick..\n"REST);
	int pivote,i,j;
	int tmp;

	if(low<high){
		pivote=low;
		i=low;
		j=high;
		while(i<j){
			// se incrementa i hasta que a[i] sea mayor al pivote
			while(a[i]<=a[pivote] && i<=high){
				i++;
				printf(BgYellow"incremento i..\n"REST);
			}
			// se decrementa j hasta que a[j] sea menor que el pivote
			while(a[j]>a[pivote] && j>=low){
				j--;
				printf(BgYellow"decremento j..\n"REST);
			}
			printf(BgYellow"\n Estado: \n"REST);
			for(int k=0; k<Max; k++){
			printf("%i ",a[k] );
			}
			printf("\n");
			// En caso de que i<j se intercambian los elementos y se se sigue en este ciclo
			if(i<j){
				tmp=a[i];
				a[i]=a[j];
				a[j]=tmp;
				printf(BgYellow"\nIntercambio i con j..\n"REST);
				for(int k=0; k<Max; k++){
					printf("%i ",a[k] );
				}
				printf("\n");
			}
		}
	// Aqui i>=j, entonces se intercambia el pivote y se llama recursivamente la función		
	tmp=a[j];
	a[j]=a[pivote];
	a[pivote]=tmp;
	printf(BgYellow"intercambio pivote..\n"REST);
	for(int k=0; k<Max; k++){
					printf("%i ",a[k] );
				}	
	printf("\n");
	// Como el pivote ya está en su lugar acomodado, se llama la función sin incluirlo
	quickSort(a,low,j-1);
	quickSort(a,j+1,high);
	}
}

