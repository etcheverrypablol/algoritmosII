interleave :: a -> [a] -> [[a]]
interleave x [] = [[x]]
interleave x (y:ys) = (x:y:ys):(map (\s -> y:s) (interleave x ys))

permutaciones :: [a] -> [[a]]
permutaciones [] = [[]]
permutaciones (x:xs) = concat (map (\s -> (interleave x s)) (permutaciones xs))

subconjuntos:: [a] -> [[a]]
subconjuntos [] = [[]]
subconjuntos (x:xs) =  (map (x:) (subconjuntos xs)) ++ subconjuntos xs

sublistas :: [a]->[[a]]
sublistas [] = [[]]
sublistas (x:xs) = [(x:xs)]++(sublistas xs) 
