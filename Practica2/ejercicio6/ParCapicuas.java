/**
 * Una clase auxiliar para guardar dos capicuas de 5 digitos que resuelven el problema.
*/
public class ParCapicuas {
    // Atributos privados de la clase
    private Capicua5D a; 
    private Capicua5D b;
    
    // Constructor que sirve para crear un par capicUa
    public ParCapicuas(Capicua5D a, Capicua5D b) {
        this.a = a;
        this.b = b;
    }
    // Metodo para retornar el primer numero capicUa
    public Capicua5D primero() {
        return this.a;
    }
    // Metodo para retornar el segundo numero capicUa
    public Capicua5D segundo() {
        return this.b;
    }
    // Metodo para mostrar el par con los dos numeros de 5 digitos
    @Override
    public String toString() {
        return "(" + this.a.toString() + ", " + this.b.toString() + ")";
    }
    
}