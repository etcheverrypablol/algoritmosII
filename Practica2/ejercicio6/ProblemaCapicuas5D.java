import java.util.List;
import java.util.LinkedList;

/*
 * Clase que resuelve el problema de los numeros capicuas.
 * <p>
 * Una solucion a este problema consiste en un par (a,b) tal que a y b son numeros capicuas de 5 digitos tal que no exista
 * otro par (c,d) de numeros capicuas de 5 digitos tal que |c - d| < |a - b|.
 * 
*/
public class ProblemaCapicuas5D {
    
    /**
     * @return todas las soluciones posibles al problema que plantea esta clase
     */ 
    public static List<ParCapicuas> obtenerSoluciones() {
       // throw new UnsupportedOperationException("ProblemaCapicuas5D#obtenerSoluciones() : no implementado");
        List<ParCapicuas> lista = new LinkedList<ParCapicuas>();
        Capicua5D capicua1 = new Capicua5D(10001);
        while(capicua1.valorEntero()<99999){
            Capicua5D capicua2 = new Capicua5D(10001);
            while(capicua2.valorEntero()<99999){
                ParCapicuas parCap = new ParCapicuas(capicua1,capicua2);
                if(!capicua1.equals(capicua2)){
                    lista.add(parCap);
                }
                capicua2=capicua2.siguiente();
            }
            capicua1=capicua1.siguiente();
        }
        return lista;
    }
    
    /*
    * @return una solucion al problema que plantea esta clase
    */ 
    public static ParCapicuas obtenerSolucion() {
        //throw new UnsupportedOperationException("ProblemaCapicuas5D#obtenerSolucion() : no implementado");
        LinkedList<ParCapicuas> lista = (LinkedList)obtenerSoluciones();
        ParCapicuas parCap = lista.getFirst();
        int diferencia = (parCap.primero()).diferencia(parCap.segundo());
        for(ParCapicuas par : lista){
            if((par.primero()).diferencia(par.segundo())<diferencia) {
                diferencia = (par.primero()).diferencia(par.segundo());
                parCap = par;
            }
        }
        return parCap;
    }
    
    
}