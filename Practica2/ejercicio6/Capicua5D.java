/*
 * Clase Capicua5D.
 * <p>
 * Representa un numero capicua de 5 digitos.
 * <p>
 * Cualquier numero de menos de 5 digitos (por ejemplo 88) puede verse como uno de 5 digitos donde los faltantes son 0 (por ejemplo 00088).
 * Pero en estos casos se deben tener en cuenta los 0's al determinar si es capicua (00088 no lo es, aunque 88 si lo es).
*/
public class Capicua5D {
    private int numero;     // Es la representacion interna que se usa para representar al numero capicua 

    // Constructor de un numero capicua
    public Capicua5D(int num){
        numero = validar(num) ? num : 10001;     
    }    

    /*
     * Obtiene el siguiente capicua de 5 digitos sin cambiar esta instancia. 
     * @return el siguiente capicua de 5 digitos o {@code null} si no existe otro.
    */
    public Capicua5D siguiente() {
        int n = valorEntero()+1;
        while(n<99999 && !(validar(n))){
            n++;
        }
       Capicua5D cap = (n>99999) ? null : new Capicua5D(n);
        return cap;
    }
    
    /*
     * Retorna la diferencia entera entre este capicua y otro.
     * <p>
     * @param otro  :   otro capicua con el cual obtener la diferencia  :   {@code Capicua5D}
     * @return {@code |valorEntero() - otro.valorEntero()|}
     * @throws IllegalArgumentException : si {@code otro} es {@code null} o no es un numero capicua 
    */
    public int diferencia(Capicua5D otro) throws IllegalArgumentException {
        //throw new UnsupportedOperationException("Capicua#diferencia(Capicua) : no implementado");
        if (otro == null || !validar(otro.valorEntero())) throw new IllegalArgumentException("Null || !Capicua");
        return Math.abs(this.valorEntero() - otro.valorEntero());
    }
    
    /*
     * @return el valor entero que representa este numero capicua   :   {@code int} 
    */
    public int valorEntero() {
        return numero;          
    }
    
    /*
     * @return un capicua cuyo valor entero sea 0   :   {@code Capicua5D} 
    */
    public static Capicua5D zero() {
        throw new UnsupportedOperationException("Capicua#zero() : no implementado");
    }
    
    /*
     * @param num   :   el numero entero a validar  :   {@code int}
     * @return {@code true} sii {@code num} es un numero de 5 digitos y es capicua.
    */
    public static boolean validar(int num) {
        if(Math.abs(num)>99999) return false;
        String numero = String.valueOf(num);
        if (numero.length()==4) numero="0"+numero;
        if (numero.length()==3) numero="00"+numero;
        if (numero.length()==2) numero="000"+numero;
        if (numero.length()==1) numero="0000"+numero;
  
        if(numero.charAt(0)==numero.charAt(numero.length()-1) && numero.charAt(1)==numero.charAt(numero.length()-2)){
            return true;
        }
        return false;
    }
    
    /*
     * @return una instancia de Capicua5D, solo si {@code num} es un numero de 5 digitos capicua.
    */
    public static Capicua5D aPartirDe(int num) {
        throw new UnsupportedOperationException("Capicua#aPartirDe(int) : no implementado");
    }
    
    @Override
    public String toString() {
        return String.format("%05d", valorEntero());
    }
    
    public boolean equals(Capicua5D otro) {
        return valorEntero() == otro.valorEntero();
    }
    

}
