/**
 * Main class of the Java program. 
 * 
 */
 
import java.util.List;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        
        System.out.println("Soluciones al problema de los numeros capicuas de 5 digitos");
        ParCapicuas par = ProblemaCapicuas5D.obtenerSolucion();
        int distancia= par.primero().diferencia(par.segundo());
        System.out.println("El par de distancia mínima (" + distancia +") entre los dos numeros es: " + par.toString());

        //System.out.println("Ingrese el numero a saber si es capicua: ");


        //System.out.println(String.valueOf(Capicua5D.validar(Integer.parseInt(args[0]))));

        /*
        for (ParCapicuas s : ProblemaCapicuas5D.obtenerSoluciones()) {
            System.out.println(s.toString() + " diferencia : " + s.primero().diferencia(s.segundo()));
        }
        */
    }
}
