import java.util.Collection;
import java.util.List;
import java.util.LinkedList;

/**
 * Esta clase representa el problema de las ocho reinas.
 * 
 * Dado un tablero de ajedrez hay que encontrar todas las formas de ubicar ocho reinas tal que ninguna amenace a otra.
 */ 
public class OchoReinas {
    
    /*
      Obtiene una solucion al problema de las ocho reinas.
      
      @return una lista de posiciones de reinas tal que ninguna reina amenaza a otra
      
    public static List<Pair<Integer, Integer>> obtenerSolucion() {
        //throw new UnsupportedOperationException("OchoReinas#obtenerSolucion() : List<Pair<Integer, Integer>> , no esta implementada");
        
    }
    
    /*
     * Obtiene todas las soluciones al problema de las ocho reinas.
     * 
     * @return una coleccion de soluciones
     * LinkedList es una implementacion de List. List es una interface.
     */
    public static Collection<LinkedList<Pair<Integer, Integer>>> obtenerSoluciones() { //List viene ya en Java
        //throw new UnsupportedOperationException("OchoReinas#obtenerSoluciones() : Collection<List<Pair<Integer, Integer>>> , no esta implementada");
        Collection<LinkedList<Pair<Integer, Integer>>> soluciones = new LinkedList(); //Lista de Lista de Pares (Podés verlo como una matriz dimensional)
        LinkedList<Pair<Integer, Integer>> lista = new LinkedList(); //Lista de Pares
        Pair<Integer, Integer> pair = new Pair(); // Se usa para asignarle valores a pares
        //for (Pair<Integer, Integer> r1 : reinas) {
        // (X,Y) TOMA 64 PARES EMPEZANDO DESDE EL (1,1)             
                for (Pair<Integer, Integer> r2 : reinas) {
                    for (Pair<Integer, Integer> r3 : reinas){
                        if(r1.getFirst() != r2.getFirst() || r1.getSecond() != r2.getSecond()){
                            if(r1.getFirst() == r2.getFirst() || r1.getSecond() == r2.getSecond() || r1.getFirst()+r1.getSecond() == r2.getFirst()+r2.getSecond() || r1.getSecond()-r1.getFirst() == r2.getSecond()-r2.getFirst()){
                             pair.setFirst(r3);
                             pair.setSecond(r3);
                             lista.add(pair);
                             }
                        }    
                    }
                    soluciones.add(lista);
                }
        //}    
    }


    /**
     * Valida que dado una lista de posiciones de reinas, ninguna amenaza a otra
     * 
     * @return {@code true} sii ninguna reina amenaza a otra
     */

    public static boolean esSolucion(List<Pair<Integer, Integer>> reinas) {
    //    throw new UnsupportedOperationException("OchoReinas#esSolucion(List<Pair<Integer, Integer>>) : boolean , no esta implementada");
        for (Pair<Integer, Integer> r1 : reinas) {
            for (Pair<Integer, Integer> r2 : reinas) {
                if(r1.getFirst() != r2.getFirst() || r1.getSecond() != r2.getSecond()){
                    if(r1.getFirst() == r2.getFirst() || r1.getSecond() == r2.getSecond() || r1.getFirst()+r1.getSecond() == r2.getFirst()+r2.getSecond() || r1.getSecond()-r1.getFirst() == r2.getSecond()-r2.getFirst()){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}