/**
 * Main class of the Java program. 
 * 
 */
 
import java.util.List;

public class Main {

    public static void main(String[] args) {
       
       //System.out.println("Solucion al problema de las ocho reinas: \n" + reinasToString(OchoReinas.obtenerSolucion()));
       
    }
    
    public static String reinasToString(List<Pair<Integer, Integer>> reinas) {
        int[][] tablero = new int[8][8];
        for (Pair<Integer, Integer> r : reinas) {
            int c = r.getFirst();
            int f = r.getSecond();
            tablero[c][f] = 1;      // Pone 1 donde hay una reina
        }
        // Esta parte muestra la solucion con un String
        String res = "- - - - - - - - -\n";
        for (int c = 0; c < 8; c++) {
            for (int f = 0; f < 8; f++) {
                res += "|" + (tablero[c][f] == 0?" ":"R"); //Pone R donde hay una reina
            res += "|\n";
            }
        res += "- - - - - - - - -\n";
        
        }
        return res;
    }
}
